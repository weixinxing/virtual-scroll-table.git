/**
 *
 * 必须加上fill(1)来进行初始化，因为map会进行（index in O）的判定
 * @type {any[]}
 */
let data = new Array(60).fill(1);

data = data.map((item1, index) => {
    let item = {};
    item.id = index;
    item.age = "姓名";
    item.address = "地址";
    return item;
});
export default data;

