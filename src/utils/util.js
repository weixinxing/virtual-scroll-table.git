/**
 * 处理空字符串的显示
 * @param str
 * @param blankWord
 * @param izMoney
 * @returns {*}
 */
export const handleBlankWord = (str, blankWord = "~", izMoney = false, izFixed = true) => {
    return (str === undefined || str === null || str === "") ? blankWord : izMoney ? toThousands(str,izFixed) : str;
};